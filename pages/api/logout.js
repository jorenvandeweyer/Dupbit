const Token = require("../../src/util/Token");
const Cookie = require("../../src/util/Cookie");

async function resolve(data, apidata) {
    await Token.removeToken(apidata.session.tid);
    return {
        success: true,
        login: false,
        backdirect: true,
        cookie: Cookie.remove("sid"),
    };
}

module.exports = {
    resolve
};
